<section class="threeBlockwithContent Section" id="threeBlockwithContent">
	<div class="container">
		<div class="centeralignHead">
			<h2>How Your Business Can Tide Through The Covid-19 Impact</h2>
			<p>As the world responds to concerns over the coronavirus outbreak, we know that this time presents uncertainty & challenges for everyone who’s dealing with this first hand- the economy, the businesses and the people it assists.</p>
		</div>
		<div class="row">
			<div class="col-12 col-sm-4">
				<div class="innerWrap">
					<img src="assets/img/tempImg/virus-slider.png" alt="covid-19_impact_img">
					<h3>Economic Impact & The Bailout</h3>
					<p>With the effects of lockdown the economy might take upto six months to recover from the effects of the outbreak. Economy is assumed to start expanding again in the second half of the year as life starts to normalize.</p>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="innerWrap">
					<img src="assets/img/tempImg/hand-shaking.png" alt="covid-19_impact_img">
					<h3>Business Impact & The Survival</h3>
					<p>With the coronavirus lockdown most businesses have come to a standstill and no cash cushion. Since nobody knows when it will be safe for people to go out again, businesses should rethink the way they reach out to their customers.</p>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="innerWrap">
					<img src="assets/img/tempImg/corona-safety.png" alt="covid-19_impact_img">
					<h3>Human Impact- Employment & Consumers</h3>
					<p>With dried up income and the risk of layoffs, job workers are at the maximum stress. While customers are practicing social distancing, businesses are facing the consequence of this with thinned out margins.</p>
				</div>
			</div>
		</div>
	</div>
</section>
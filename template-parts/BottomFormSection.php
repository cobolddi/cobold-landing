<section class="StepByStepForm" id="StepByStepForm">
	<div class="container">
		<div class="ShadowBox">
			<div class="FormBlock">
				<h2>Get Step-By-Step Help On Your Business Recovery</h2>
				<form id="ajax-contact" method="post" action="mail.php">
					<div class="row">
						<div class="col-12 col-md-6">
							<input type="text" id="fname" name="fname" placeholder="First Name" required>
						</div>
						<div class="col-12 col-md-6">
							<input type="text" id="lname" name="lname" placeholder="Last Name" required>
						</div>
						<div class="col-12 col-md-6">
							<input type="email" id="email" name="email" placeholder="Email ID">
						</div>
						<div class="col-12 col-md-6">
							<input type="text" id="phone" name="phone" placeholder="Phone No.">
						</div>
						<div class="col-12">
							<textarea id="message" name="message" placeholder="Message"></textarea>
						</div>
						<div class="col-12">
							<input type="submit" value="Send Message" class="">
						</div>
						<div id="form-messages"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
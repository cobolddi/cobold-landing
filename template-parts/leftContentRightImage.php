<section class="leftContentRightImage" id="AboutCobold">
	<div class="container-fluid">
		<div class="row">	
			<div class="col-12 col-md-6 px-0">
				<div class="innerWrap">
						<h2>About Cobold Digital</h2>
						
						<p>We are building a bridge between businesses and the digital world through design innovation. Since the inception, we have accelerated the growth of business with our designs, branding and market-penetration strategies.</p>
						<p>We help create truly learning, responsive and people-centric web experiences.</p>
						 <p>Our solutions cover an entire spectrum of digital transformations for our clients, taking care of all their business needs!</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-0">
				<img src="assets/img/tempImg/cobold_team.png" alt="cobold_team">
			</div>
		</div>
	</div>
</section>
<section class="banner" id="banner">
    <div class="bannerSlider">
        <div class="bannerWrapper">
            <div class="bannerImage">
                <img src="assets/img/tempImg/banner1.png" alt="">
            </div>
            <div class="bannerDetailWrapper">
                <div class="container">
                    <div class="bannerDetails">
                    	<p>Is COVID-19 making your business very sick right now?</p>
                        <h1>Help Your Business Survive <br> The Covid-19 Crisis</h1>
                        <a href="#" class="btn cta-btn-transparent">Get in Touch</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="bannerWrapper">
            <div class="bannerImage">
                <img src="assets/img/tempImg/banner2.png" alt="">
            </div>
            <div class="bannerDetailWrapper">
                <div class="container">
                    <div class="bannerDetails">
                    	<p>Is COVID-19 making your business very sick right now?</p>
                        <h1>Help Your Business Survive <br> The Covid-19 Crisis</h1>
                        <a href="#" class="btn cta-btn-transparent">Get in Touch</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
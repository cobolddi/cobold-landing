<section class="OnlineBusinessBlock" id="OnlineBusinessBlock">
	<div class="container">
		<div class="ContentBlock">
			<h5>The internet is currently the only corona-proof business space.</h5>
			<h2>Take Your Business Online</h2>
			<p>When the world is maintaining social distancing, it’s the internet that’s keeping everyone<br> connected. Prepare your business to be always present for your customers!<br> Bring your business online with us!</p>
		</div>
		<div class="TopIconBottomContent">
			<div class="row">
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/web.svg" alt="">
						<h5>Get A Business Website</h5>
						<p>Sell your products to buyers from your online store with a website.</p>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/app.svg" alt="">
						<h5>Get A Business Application</h5>
						<p>Get web or mobile apps, where people come to interact with your brand.</p>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/social.svg" alt="">
						<h5>Get Promoted On Social</h5>
						<p>Take your brand social so that brand is always connected with your target community.</p>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/brand.svg" alt="">
						<h5>Branding & Design</h5>
						<p>Create a recall value for your brand with a striking brand identity.</p>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/service.svg" alt="">
						<h5>Get A Maintenance Service</h5>
						<p>Have your social assets run swiftly without any hassle with our reliable maintenance service.</p>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/consult.svg" alt="">
						<h5>Get Custom Consulting</h5>
						<p>Need to resolve business issues with an expert’s advice? We have one for you!</p>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/retain.svg" alt="">
						<h5>Monthly Retainer Service</h5>
						<p>On-demand service to keep up with all your maintenance needs.</p>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div class="IconContentBox">
						<img src="assets/img/devops.svg" alt="">
						<h5>DevOps</h5>
						<p>Delivery at pace of business with emphasis on communication, integration & automation.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
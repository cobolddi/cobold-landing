<section class="OurClients" id="OurClients">
	<div class="container">
		<h2>All The Clients We Are Keeping Connected With The World</h2>
		<div class="TeamCards">
            <div class="row">
                <div class="col-6 col-md-4 col-sm-4">
                    <div class="CardsBlock">
                        <div class="ImgBox">
                            <img src="assets/img/merino.png" alt="">
                        </div>                      
                        <div class="ContentBox">
                            <a href="http://www.merinolaminates.com/" target="_blank">View More</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-sm-4">
                    <div class="CardsBlock">
                        <div class="ImgBox">
                            <img src="assets/img/merino-adcc.png" alt="">
                        </div>  
                        <div class="ContentBox">
                            <a href="http://merinoadcc.com/" target="_blank">View More</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-sm-4">
                    <div class="CardsBlock">
                        <div class="ImgBox">
                            <img src="assets/img/ndim.png" alt="">
                        </div>  
                        <div class="ContentBox">
                            <a href="https://ndimdelhi.org" target="_blank">View More</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-sm-4">
                    <div class="CardsBlock">
                        <div class="ImgBox">
                            <img src="assets/img/max.png" alt="">
                        </div>  
                        <div class="ContentBox">
                            <a href="http://maxassets.in/" target="_blank">View More</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-sm-4">
                    <div class="CardsBlock">
                        <div class="ImgBox">
                            <img src="assets/img/mmt.png" alt="">
                        </div>  
                        <div class="ContentBox">
                            <a href="http://www.makemytrip.com/about-us/work-from-home.php" target="_blank">View More</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-sm-4">
                    <div class="CardsBlock">
                        <div class="ImgBox">
                            <img src="assets/img/ush-cook.png" alt="">
                        </div>  
                        <div class="ContentBox">
                            <a href="http://maxassets.in/" target="_blank">View More</a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="ExternalLink">More Work</a>
        </div>
	</div>
</section>
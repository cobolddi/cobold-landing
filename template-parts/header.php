<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cobold Digital</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Help you Business Survive the covid-19 Crisis">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	   <link rel="icon" type="image/x-icon" href="assets/img/tempImg/fav-icon.png">
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header class="DesktopMenu DesktopOnly">
    <div class="container">
        <nav class="navbar">
            <div class="brand-title">
                <a href="cobold.in" target="_blank">
                	<img src="assets/img/tempImg/cobold-logo.png" alt="Cobold-Logo">
                </a>
            </div>
            <!-- .site-branding -->
            <div class="navbar-links">
                    <ul>
                        <li>
                        	<a href="#AboutCobold" class="anchor-link">ABOUT</a>
                        </li>
                        <li>
                        	<a href="#threeBlockwithContent" class="anchor-link">SCENARIOS</a>
                        </li>
                        <li>
                            <a href="#OnlineBusinessBlock" class="anchor-link">SERVICES</a>
                         </li>
                         <li>
                            <a href="#OurClients" class="anchor-link">WORKS</a>
                         </li>
                        <li>
                        	<a href="#StepByStepForm" class="anchor-link">CONTACT</a>
                        </li>
                    </ul>
            </div>
        </nav>
        <!-- #site-navigation -->
    </div>
</header>


<header class="MobileMenu MobileOnly">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul>
         <li>
            <div class="brand-title">
                <a href="cobold.in" target="_blank">
                    <img src="assets/img/tempImg/cobold-logo.png" alt="Cobold-Logo">
                </a>
            </div>
          </li>
          <li>
            <a class="" href="#my-menu">
              <span class="MenuButton">
                  <div class="bar"></div>
                  <div class="bar"></div>
                  <div class="bar"></div>
              </span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
<!-- Mobile Menu -->
  <div id="my-page">
      <div id="my-header">
          <nav id="my-menu">
              <ul>
                        <li>
                          <a href="#AboutCobold" class="anchor-link">ABOUT</a>
                        </li>
                        <li>
                          <a href="#threeBlockwithContent" class="anchor-link">SCENARIOS</a>
                        </li>
                        <li>
                            <a href="#OnlineBusinessBlock" class="anchor-link">SERVICES</a>
                         </li>
                         <li>
                            <a href="#OurClients" class="anchor-link">WORKS</a>
                         </li>
                        <li>
                          <a href="#StepByStepForm" class="anchor-link">CONTACT</a>
                        </li>
                    </ul>
          </nav>
      </div>
  </div>
</header>
<main>
</main>

<footer>
	<div class="TopFooterMenu">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-3">
					<div class="footerLogo">
						<a href="cobold.in" target="_blank"><img src="assets/img/logo-white.svg" alt=""></a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ac gravida dolor, sed convallis turpis.</p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<h4>Quick Links</h4>
					<p>Stay update with our latest</p>
					<form action="mailto:cobolddigital@gmail.com">
						<input type="email" placeholder="Enter Email">
						<span><input type="submit" value=""></span>
					</form>
				</div>
				<div class="col-12 col-md-3">
					<h4>Get In touch</h4>
					<div class="contactLinks">
						<div class="innerlinks">
							<svg>
		                        <use xlink:href="assets/img/cobold-sprite.svg#icon-Phone-Number"></use>
		                    </svg>
		                    <a href="tel:+919871069987">+91 987 106 9987</a>
						</div>
						<div class="innerlinks">
							<svg>
		                        <use xlink:href="assets/img/cobold-sprite.svg#icon-Email-Address"></use>
		                    </svg>
		                    <a href="mailto:hi@cobold.co">hi@cobold.co</a>
						</div>
						<div class="innerlinks">
							<svg>
		                        <use xlink:href="assets/img/cobold-sprite.svg#icon-Location-Maps"></use>
		                    </svg>
		                    <a href="#">Farm No. 4, Second Floor, Club Drive, Ghitorni <br>New Delhi India 110030</a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-2">
					<h4>Follow Us</h4>
					<h6>Let us be social</h6>
					<div class="socialmedia">
						<a href="#">
							<svg class="svgClass">
		                        <use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use>
		                    </svg>
		                </a>
						<a href="#">
							<svg class="svgClass">
		                        <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
		                    </svg>
		                </a>
		                <a href="#">
							<svg class="svgClass">
		                        <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
		                    </svg>
		                </a>
		                <a href="#">
							<svg class="svgClass">
		                        <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
		                    </svg>
		                </a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="CopyrightBlock">
		<div class="container">
			<p>Copyright © 2020 All rights reserved Cobold Digital l.l.p.</p>
		</div>
	</div>
</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>
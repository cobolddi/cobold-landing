<section class="leftImageRightContent" id="PostCorona">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-6 px-0">
				<img src="assets/img/tempImg/business_post_corona.png" alt="business_after_corona">
			</div>
			<div class="col-12 col-md-6 px-0">
				<div class="innerWrap">
						<h2>Is Your Business Ready For The Post-Corona World?</h2>
						<p>The ever-changing situation surrounding the Coronavirus (COVID19) pandemic is forcing businesses to rethink how they operate, how they can better understand the customer expectations and what actions they should take to improve their handling of the situation.</p>
						<ul class="circleList">
							<li>Is your business limited to a physical store without a website?</li>
							<li>How are you doing social media in the times of social distancing?</li>
							<li>Are you keeping everyone informed about your business?</li>
							<li>Are you staying connected with your customers during quarantine?</li>
							<li>Are you continuously present virtually when the physical is an uncertainly?</li>
						</ul>
				</div>
			</div>
		</div>
	</div>
</section>
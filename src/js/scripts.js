
$(window).on('load', function () {
  AOS.refresh();
});
$(function () {
  AOS.init({
    easing: 'ease-in-out',
    mirror: false,
    duration:500,
    once: true
  });
});

$(document).ready(function() {
     $('.lazy').Lazy({
        effect: 'fadeIn',
        visibleOnly: true,
        scrollDirection: 'vertical'
     });


 $(document).ready(function() {

// Banner slider
$(".bannerSlider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots:false, 
      arrow: false,
      fade: true,
      autoplay: true,
      pauseOnHover: false,
      speed: 800,
      infinite: true,
      cssEase: 'ease-in-out',
      touchThreshold: 100
    });

    

    $(".anchor-link").on("click", function( e ) {

    e.preventDefault();

    $("body, html").animate({ 
      scrollTop: $( $(this).attr('href') ).offset().top 
    }, 600);

  });
    

    //Section Scroll on click on nav links
    function scrollNav() {
      $('.navbar-links ul li>a').click(function(){  
        //Toggle Class
        $(".active").removeClass("active");      
        $(this).closest('li').addClass("active");
        var theClass = $(this).attr("class");
        $('.'+theClass).parent('li').addClass('active');
        //Animate
        $('html, body').stop().animate({
            scrollTop: $( $(this).attr('href') ).offset().top - 60
        }, 800);
        return false;
      });
      $('.scrollTop a').scrollTop();
    }
    scrollNav();


//M-Menu Lite 
 document.addEventListener(
  "DOMContentLoaded", () => {
      const menu = new MmenuLight(
          document.querySelector( "#my-menu" ), 
          "(max-width: 767px)"
      );

      const navigator = menu.navigation();
      const drawer = menu.offcanvas();

      document.querySelector( "a[href='#my-menu']" )
          .addEventListener( "click", ( evnt ) => {
              evnt.preventDefault();
              drawer.open();
          });
  }
);


 $(function() {
      // Get the form.
      var form = $('#ajax-contact');

      // Get the messages div.
      var formMessages = $('#form-messages');

      // TODO: The rest of the code will go here...
  });

   // Set up an event listener for the contact form.
  $(form).submit(function(event) {
      // Stop the browser from submitting the form.
      event.preventDefault();

      // TODO
  });

  var formData = $(form).serialize();

  // Submit the form using AJAX.
  $.ajax({
      type: 'POST',
      url: $(form).attr('action'),
      data: formData
  })

  .done(function(response) {
      // Make sure that the formMessages div has the 'success' class.
      $(formMessages).removeClass('error');
      $(formMessages).addClass('success');

      // Set the message text.
      $(formMessages).text(response);

      // Clear the form.
      $('#fname').val('');
      $('#lname').val('');
      $('#phone').val('');
      $('#email').val('');
      $('#message').val('');
  })

  .fail(function(data) {
      // Make sure that the formMessages div has the 'error' class.
      $(formMessages).removeClass('success');
      $(formMessages).addClass('error');

      // Set the message text.
      if (data.responseText !== '') {
          $(formMessages).text(data.responseText);
      } else {
          $(formMessages).text('Oops! An error occured and your message could not be sent.');
      }
  });


});






